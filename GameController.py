from Action import *
from Character import *
from Place import *
from Item import *
from Dialog import *
from Leave import *
from Inventory import *
from TeluguDialog import *

class ShowMenu:
    def __init__(self, char_objects):
        self.char_objects = char_objects

    def showMenu(self):
        PlaySound('Menu', self.char_objects['Dwight'].name)
        action('ShowMenu()')

    def hideMenu(self):
        PlaySound('Menu', self.char_objects['Dwight'].name)
        action('HideMenu()')

    def enableInput(self):
        action('EnableInput()')

    def disableInput(self):
        action('DisableInput()')

    def credits(self):
        PlaySound('Button', self.char_objects['Dwight'].name)
        action('ShowCredits()')

    def hideCredits(self):
        PlaySound('Button', self.char_objects['Dwight'].name)
        action('HideCredits()')

    def quit(self):
        PlaySound('Button', self.char_objects['Dwight'].name)
        action('Quit()')

class GameController(ShowMenu):

    game_inputs =  None
    def __init__(self, camelot_config, game_inputs):
        self.chest_inventory = None
        self.place_names = camelot_config.get("places")
        self.char_names = camelot_config.get("characters").get("names")
        self.item_names = camelot_config.get("items")
        self.enable_icons = camelot_config.get("enable")
        self.narration_message = camelot_config.get("narration_message")
        self.camera_focus = camelot_config.get("set_camera_focus")
        self.game_inputs = game_inputs
        self.item_objects = dict()
        self.char_objects = dict()
        self.place_objects = dict()
        self.initialize( )
        setCameraFocus(self.camera_focus)
        self.showMenu()
        # flags for game milestones
        self.flags = camelot_config.get('game_control_flags')



    def initialize(self):
        self.create_places(self.place_names)
        self.create_chars( self.char_names )
        self.create_items(self.item_names)
        self.enable_objects(self.enable_icons)

        self.chest_inventory = Inventory('Dungeon.Chest')
        self.chest_inventory.add(self.item_objects['coin'], "The sacred coin that everyone envies.")
        self.chest_inventory.add(self.item_objects['dungeon_key'], "This key opens the door to the Dungeon.")
        self.chest_inventory.add(self.item_objects['bag_of_gold'], "A bag of gold.")

        self.dialog = Dialog()
        self.telugu = TeluguDialog()
        self.inventory = Inventory("Dwight")
        super().__init__(self.char_objects)
        enableIcon('Open_Dungeon_Door', 'Open', 'Dungeon.Door', "Open Door", True)
        Pocket(self.char_objects['Kevin'], self.item_objects['kevin_sword'])
        Pocket(self.char_objects['Bandit'], self.item_objects['bandit_sword'])

    def create_places(self, place_names):
        for place in place_names:
            self.place_objects[place] = Place(place, place)

    def create_chars(self, char_names):
        for char in char_names:
            self.char_objects[char.get("name")] = Character(char.get("name"), char.get("body_properties").get("body_type"))
            self.char_objects[char.get("name")].set_outfit(char.get("body_properties").get("hair_style"),
                                                           char.get("body_properties").get("outfit"))
            self.char_objects[char.get("name")].set_position(char.get("position"))

    def create_items(self, item_names):
        for item in item_names:
            self.item_objects[item.get("item_name")] = Item(item.get("item_name"), item.get("name"))
            self.item_objects[item.get("item_name")].set_position(item.get("position"))

    def enable_objects(self, enable_params):
        for val in enable_params.values():
            for object_list in val:
                name = object_list.get('name')
                action_name = object_list.get('params').get("action_name")
                action = object_list.get('params').get("action")
                message = object_list.get('params').get("message")
                enableIcon(action_name, action, name, message, True)

    def setCameraFocus(self, entity_name):
        action(f'SetCameraFocus({entity_name})')

    def setCameraMode(self, mode):
        action(f'SetCameraMode({mode})')


    def start(self):
        PlaySound('Button', self.char_objects['Dwight'].name)
        self.hideMenu()
        # Opening scene narration message
        self.showNarration(self.narration_message)
        self.enableInput()
        PlaySound('LivelyMusic', self.char_objects['Dwight'].name, 'true')

    def showNarration(self, message):
        action(f'SetNarration("{message}")')
        action('ShowNarration()')

    def hideNarration(self):
        action('HideNarration()')

    def showInventory(self):
        self.inventory.show()

    def hideInventory(self):
        if self.flags.get('found_dungeon_chest'):
            self.flags['found_dungeon_chest'] = False
            self.inventory.hide()
            self.showNarration(
                'You found the dungeon key, a bag of gold, and the sacred coin! Press I to access these items in your inventory.')
            self.chest_inventory.add(self.item_objects['dungeon_key'], "This key opens the door to the Dungeon.")
            self.chest_inventory.add(self.item_objects['coin'], "The sacred coin.")
            self.chest_inventory.add(self.item_objects['bag_of_gold'], "A bag of gold.")
            disableIcon('Take_Dungeon_Key', self.item_objects['dungeon_key'].item_name)
            disableIcon('Take_Coin', self.item_objects['coin'].item_name)
            disableIcon('Take_Bag_Of_Gold', self.item_objects['bag_of_gold'].item_name)
        else:
            self.inventory.hide()

    def openCellDoor(self):
        if self.flags.get('found_cell_key'):
            Open(self.char_objects['Dwight'], "Dungeon.CellDoor")
            Pocket(self.char_objects['Dwight'], self.item_objects['cell_key'])
            enableIcon('Open_Dungeon_Chest', 'Open', 'Dungeon.Chest', "Open Chest", True)
            enableIcon('Open_Dungeon_Door', 'Open', 'Dungeon.Door', "Open Door", True)
        else:
            self.showNarration("The cell door is locked! Try to find something in your cell that can help set you free.")

    def takeDungeonPotion(self):
        Take(self.char_objects['Dwight'], self.item_objects['dungeon_potion'], 'Dungeon.RoundTable')
        EnableEffect(self.item_objects['dungeon_potion'].item_name, 'Poison')
        action('Wait(1)')
        DisableEffect(self.item_objects['dungeon_potion'].item_name, 'Poison')
        if not self.flags.get('found_dungeon_potion'):
            self.flags['found_dungeon_potion'] = True
        self.showNarration("This potion is deadly! Another prisoner must have left this.")

    def talkToGuard(self):
        action(f'WalkTo(Guard, Dungeon.CellDoor)')
        if self.flags['found_dungeon_potion']:
            outcome = self.dialog.guardAfterPotion(self.char_objects['Dwight'], self.char_objects['Guard'])
        else:
            if self.flags.get('language') == 'English':
                outcome = self.dialog.guardBeforePotion(self.char_objects['Dwight'], self.char_objects['Guard'])
            else:
                outcome =self.telugu.guardBeforePotion(self.char_objects['Dwight'], self.char_objects['Guard'])
        if outcome:
            self.disableInput()
            PlaySound('Unlock', 'Dungeon.CellDoor')
            Open(self.char_objects['Guard'], 'Dungeon.CellDoor')
            action(f'WalkToSpot(Guard,-305.6,0.0,-1.2)')
            Close(self.char_objects['Guard'], 'Dungeon.CellDoor')
            action(f'WalkToSpot(Guard,-302.6,0.0,-2.2)')
            Give(self.char_objects['Dwight'], self.item_objects['dungeon_potion'], self.char_objects['Guard'])
            PlaySound('Potion', self.char_objects['Guard'].name)
            action(f'Drink(Guard)')
            PlaySound('Death2', self.char_objects['Guard'].name)
            action(f'Die(Guard)')
            EnableEffect(self.char_objects['Guard'].name, 'Death')
            action('Wait(1)')
            DisableEffect(self.char_objects['Guard'].name, 'Death')
            disableIcon('Talk_Guard', self.char_objects['Guard'].name)
            self.item_objects['cell_key'] = Item('CellKey', 'RedKey')
            self.item_objects['cell_key'].set_position('Dungeon.RoundTable')
            enableIcon('Take_Cell_Key', 'Select', self.item_objects['cell_key'].item_name, "Take Cell Key", True)
            self.game_inputs['input Take_Cell_Key CellKey'] =  {'name': 'CellKey', 'params': {'action': 'takeCellKey', 'action_name': 'Take_Cell_Key', 'message': 'Take Cell Key'}}
            # enable open chest action and open door action
            enableIcon('Open_Dungeon_Chest', 'Open', 'Dungeon.Chest', "Open Chest", True)
            self.game_inputs['input Open_Dungeon_Chest Dungeon.Chest'] =  {'name': 'Dungeon.Chest', 'params': {'action': 'openDungeonChest', 'action_name': 'Open_Dungeon_Chest', 'message': 'Open Chest'}}
            enableIcon('Open_Dungeon_Door', 'Open', 'Dungeon.Door', "Open Door", True)
            self.game_inputs['input Open_Dungeon_Door Dungeon.Door'] =  {'name': 'Dungeon.Door', 'params': {'action': 'openDungeonDoor', 'action_name': 'Open_Dungeon_Door', 'message': 'Open Door'}}
            self.enableInput()
            StopSound('LivelyMusic', self.char_objects['Dwight'].name)
            PlaySound('Danger3', self.char_objects['Dwight'].name)
        else:
            action(f'WalkTo(Guard, Dungeon.Chair)')

    def takeCellKey(self):
        Take(self.char_objects['Dwight'], self.item_objects['cell_key'], 'Dungeon.RoundTable')
        self.flags['found_cell_key'] = True
        self.showNarration("The guard has the cell and chest key! Take it to free yourself and open the chest!")

    def takeDungeonKey(self):
        self.chest_inventory.remove(self.item_objects['dungeon_key'])
        self.flags['found_dungeon_key'] = True

    def takeCoin(self):
        self.chest_inventory.remove(self.item_objects['coin'])

    def takeBagOfGold(self):
        self.chest_inventory.remove(self.item_objects['bag_of_gold'])
        self.flags['have_gold'] = True

    def openDungeonDoor(self):
        if self.flags.get('found_dungeon_key'):
            Leave(self.char_objects['Dwight'], 'Dungeon.Door', 'City.BlueHouseDoor')
            StopSound('Danger3', self.char_objects['Dwight'].name)
            PlaySound('Town_Day', self.place_objects['City'].name, 'true')
        else:
            self.showNarration('This door is locked!')

    def openDungeonChest(self):
        if self.flags.get('found_cell_key'):
            Open(self.char_objects['Dwight'], 'Dungeon.Chest')
            self.chest_inventory.show()
            self.flags['found_dungeon_chest'] = True
            enableIcon('Take_Dungeon_Key', 'Take', self.item_objects['dungeon_key'].item_name, "Take Dungeon Key", True)
            self.game_inputs['input Take_Dungeon_Key dungeon_key'] =  {'name': 'dungeon_key', 'params': {'action': 'takeDungeonKey', 'action_name': 'Take_Dungeon_Key', 'message': 'Take Dungeon Key'}}
            enableIcon('Take_Coin', 'Take', self.item_objects['coin'].item_name, "Take Coin", True)
            self.game_inputs['input Take_Coin coin'] =  {'name': 'coin', 'params': {'action': 'takeCoin', 'action_name': 'Take_Coin', 'message': 'Take Coin'}}
            enableIcon('Take_Bag_Of_Gold', 'Take', self.item_objects['bag_of_gold'].item_name, "Take Bag of Gold", True)
            self.game_inputs['input Take_Bag_Of_Gold bag_of_gold'] = {'name': 'bag_of_gold', 'params': {'action': 'takeBagOfGold', 'action_name': 'Take_Bag_Of_Gold', 'message': 'Take Bag of Gold'}}
            self.game_inputs['input Close List'] = {'name': 'Inventory', 'params': {'action': 'hideInventory', 'action_name': 'Close List', 'message': 'Close List'}}
            self.game_inputs['input Key Inventory'] = {'name': 'Inventory', 'params': {'action': 'showInventory', 'action_name': 'Key Inventory', 'message': 'Key Inventory'}}
        else:
            self.showNarration('This chest is locked!')

    def openBlacksmith(self):
        Leave(self.char_objects['Dwight'], 'City.GreenHouseDoor', 'Blacksmith.Door')
        PlaySound('LivelyMusic', self.place_objects['Blacksmith'].name, 'true')

    def openCottage(self):
        Leave(self.char_objects['Dwight'], 'City.BrownHouseDoor', 'Cottage.Door')
        PlaySound('Danger2', self.place_objects['Cottage'].name, 'true')

    def exitBlacksmith(self):
        Leave(self.char_objects['Dwight'], 'Blacksmith.Door', 'City.GreenHouseDoor')
        PlaySound('Town_City', self.place_objects['City'].name, 'true')

    def talkToCreed(self):
        action(f'WalkTo(Creed, Dwight)')
        if self.flags.get('have_gold'):
            flag, choice = self.dialog.talkCreedGold(self.char_objects['Dwight'], self.char_objects['Creed'])
            if flag:
                Give(self.char_objects['Dwight'], self.item_objects['bag_of_gold'], self.char_objects['Creed'])
                self.flags['have_gold'] = False
                if self.item_objects['Sword'].item_name == choice:
                    Give(self.char_objects['Creed'], self.item_objects['Sword'], self.char_objects['Dwight'])
                    Pocket(self.char_objects['Dwight'], self.item_objects['Sword'])
                elif self.item_objects['spellbook'].item_name == choice:
                    Give(self.char_objects['Creed'], self.item_objects['spellbook'], self.char_objects['Dwight'])
                    Pocket(self.char_objects['Dwight'], self.item_objects['spellbook'])
                elif self.item_objects['blacksmith_potion'].item_name == choice:
                    Give(self.char_objects['Creed'], self.item_objects['blacksmith_potion'], self.char_objects['Dwight'])
                    Pocket(self.char_objects['Dwight'], self.item_objects['blacksmith_potion'])
                self.flags['bought_weapon'] = True
                self.chest_inventory.remove(self.item_objects['bag_of_gold'])
        else:
            self.dialog.talkCreedNoGold(self.char_objects['Dwight'], self.char_objects['Creed'])
        self.enableInput()

    def talkToJim(self):
        action(f'WalkTo(Jim, Dwight)')
        if self.flags.get('bought_weapon'):
            self.dialog.jimAfterWeapon(self.char_objects['Dwight'], self.char_objects['Jim'])
            StopSound('Ominous', self.place_objects['Ruins'].name)
            PlaySound('Danger3', self.place_objects['Ruins'].name)
            self.setCameraMode('Track')
            Draw(self.char_objects['Dwight'], self.item_objects['Sword'])
            action(f'Attack(Dwight, Jim, false)')
            PlaySound('DarkMagic', self.char_objects['Jim'])
            action(f'Cast(Jim, Dwight, red)')
            action(f'Attack(Dwight, Jim, true)')
            action(f'Kneel(Jim)')
            StopSound('Danger3', self.place_objects['Ruins'].name)
            self.dialog.jimDefeated(self.char_objects['Dwight'], self.char_objects['Jim'])
            self.setCameraMode('Follow')
            self.setCameraFocus('Dwight')
            Pocket(self.char_objects['Dwight'], self.item_objects['Sword'])
            enableIcon('Put_Coin', 'Put', 'Ruins.Altar', 'Put coin on altar', True)
            self.game_inputs['input Put_Coin Ruins.Altar'] = {'name': 'Put_Coin', 'params': {'action': 'winGame', 'action_name': 'Put_Coin', 'message': 'Put coin on altar'}}
            self.enableInput()
        else:
            self.dialog.jimBeforeWeapon(self.char_objects['Dwight'], self.char_objects['Jim'])
            PlaySound('Danger3', self.place_objects['Ruins'].name)
            self.setCameraMode('Track')
            action(f'Attack(Dwight, Jim, false)')
            PlaySound('DarkMagic', self.char_objects['Jim'].name)
            action(f'Cast(Jim, Dwight, red)')
            Die(self.char_objects['Dwight'])
            self.dialog.loseMessage()
            self.credits()
            PlaySound("Tavern", 'true')

    def talkToPam(self):
        action(f'WalkTo(Pam, Dwight)')
        self.dialog.talkPam(self.char_objects['Dwight'], self.char_objects['Pam'])

    def talkToBandit(self):
        action(f'WalkTo(Bandit, Dwight)')
        choice = self.dialog.talkBandit(self.char_objects['Dwight'], self.char_objects['Bandit'])
        if choice and self.flags.get('bought_weapon'):
            Draw(self.char_objects['Dwight'], self.item_objects['Sword'])
            Draw(self.char_objects['Bandit'], self.item_objects['bandit_sword'])
            action('SetCamerBlend(2)')
            Fight(self.char_objects['Dwight'], self.char_objects['Bandit'])
            Die(self.char_objects['Bandit'])
            Pocket(self.char_objects['Dwight'], self.item_objects['Sword'])
            disableIcon('Talk_Bandit', 'Bandit')
            StopSound('Danger2', self.place_objects['Cottage'].name)

    def talkToMichael(self):
        action(f'WalkTo(Michael, Dwight)')
        if self.flags.get("bought_weapon"):
            self.dialog.talkToMichael_after_weapon(self.char_objects['Dwight'], self.char_objects['Michael'])
        elif self.dialog.talkMichael(self.char_objects['Dwight'], self.char_objects['Michael']):
            Give(self.char_objects['Dwight'], self.item_objects['coin'], self.char_objects['Michael'])

    def talkToKevin(self):
        action(f'WalkTo(Kevin, Dwight)')
        if self.flags.get('bought_weapon'):
            self.dialog.talkKevinWeapon(self.char_objects['Dwight'], self.char_objects['Kevin'])
            Draw(self.char_objects['Dwight'], self.item_objects['Sword'])
            Draw(self.char_objects['Kevin'], self.item_objects['kevin_sword'])
            Fight(self.char_objects['Dwight'], self.char_objects['Kevin'])
            Die(self.char_objects['Kevin'])
            disableIcon('Talk_Kevin', 'Kevin')
            Pocket(self.char_objects['Dwight'], self.item_objects['Sword'])
        else:
            self.dialog.talkKevinNoWeapon(self.char_objects['Dwight'], self.char_objects['Kevin'])
            Give(self.char_objects['Dwight'], self.item_objects['bag_of_gold'], self.char_objects['Kevin'])

    def winGame(self):
        Put(self.char_objects['Dwight'], self.item_objects['coin'], 'Ruins.Altar')
        EnableEffect(self.item_objects['coin'].item_name, 'Brew')
        action('Wait(1)')
        self.item_objects['coin'] = Item('Coin', 'Coin')
        self.item_objects['coin'].set_position('Dungeon.Chest')
        Die(self.char_objects['Jim'])
        EnableEffect(self.char_objects['Jim'].name, 'Death')
        action('Wait(1)')
        self.char_objects['Jim'] = Character('Jim', 'F')
        self.char_objects['Jim'].set_outfit('Mage_Full', 'Warlock')
        self.char_objects['Jim'].set_position('Dungeon')
        action(f'WalkTo(Pam, Dwight)')
        self.dialog.pamWinGame(self.char_objects['Dwight'], self.char_objects['Pam'])
        self.showNarration(
            'You have freed Scranton from that evil, evil, evil man Jim. The Dark lord will never again regain control or steal the identities of anybody he crosses. Dwight contemplates his success with his only thought in mind being \'bears eat beats...\'')
        self.credits()
        PlaySound("Tavern", 'true')

    def cityToForest(self):
        Leave(self.char_objects['Dwight'], 'City.NorthEnd', 'ForestPath.WestEnd')
        PlaySound('Forest_Day', self.place_objects['ForestPath'].name, 'true')

    def forestToCity(self):
        Leave(self.char_objects['Dwight'], 'ForestPath.WestEnd', 'City.NorthEnd')
        PlaySound('Town_Day', self.place_objects['City'].name, 'true')

    def forestToRuins(self):
        Leave(self.char_objects['Dwight'], 'ForestPath.EastEnd', 'Ruins.Exit')
        PlaySound('Ominous', self.place_objects['Ruins'].name, 'true')

    def cottageToCity(self):
        Leave(self.char_objects['Dwight'], 'Cottage.Door', 'City.BrownHouseDoor')
        PlaySound('Town_Day', self.place_objects['City'].name, 'true')

    def interact_lang(self):
        if self.flags.get('language') == 'English':
            action(f'SetDialog(Language selected [English | English]. want change language choose below list. \\n[English | English] \\n[Telugu | Telugu])')
            action('ShowDialog()')
        else:
            action(f'SetDialog(Language selected [Telugu | Telugu]. want change language choose below list. \\n[English | English] \\n[Telugu | Telugu])')
            action('ShowDialog()')
        choice = waitForChoice(['Telugu', 'English'])
        action('HideDialog()')
        if choice == 'English':
            self.flags['language'] = 'English'
        else:
            self.flags['language'] = 'Telugu'



