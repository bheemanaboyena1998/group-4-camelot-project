from Inventory import *
from Action import *
import unittest
from io import StringIO
from unittest.mock import patch

chest_inventory = Inventory('Dungeon.Chest')

class Test_Inventory(unittest.TestCase):

    def test_show(self):
        with patch('builtins.input', return_value='succeeded ShowList(Dungeon.Chest)'), patch('sys.stdout', new=StringIO()) as fake_out:
            self.assertEqual(chest_inventory.show(), None)

    def test_hide(self):
        with patch('builtins.input', return_value='succeeded HideList()'), patch('sys.stdout', new=StringIO()) as fake_out:
            self.assertEqual(chest_inventory.hide(), None)


if __name__ == "__main__":
    unittest.main()
