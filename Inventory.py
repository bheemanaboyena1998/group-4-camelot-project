from Place import *
from Action import *

class Inventory():

    def __init__(self, name):
        self.flag_found_dungeon_chest = None
        self.name = name
        self.inventory = []

    def inventory_initialize(self, item_objects, chest_inventory):
        self.item_objects = item_objects
        self.flag_found_dungeon_chest = False
        self.chest_inventory = chest_inventory

    def add(self, item, description):
        action(f'AddToList({item.item_name}, "{description}")')
        self.inventory.append(item)
    
    def remove(self, item):
        action(f'RemoveFromList({item.item_name})')
        self.inventory.remove(item)

    def clear(self):
        action(f'ClearList()')
        self.inventory = []

    def show(self):
        action(f'ShowList({self.name})')
    
    def hide(self):
        action(f'HideList()')

    def showInventory(self):
        self.show()

    def hideInventory(self):
        if self.flag_found_dungeon_chest:
            self.flag_found_dungeon_chest = False
            self.hide()
            self.showNarration('You found the dungeon key, a bag of gold, and the sacred coin! Press I to access these items in your inventory.')
            self.chest_inventory.add(self.item_objects['BlueKey'], "This key opens the door to the Dungeon.")
            self.chest_inventory.add(self.item_objects['Coin'], "The sacred coin.")
            self.chest_inventory.add(self.item_objects['Bag'], "A bag of gold.")
            disableIcon('Take_Dungeon_Key', 'BlueKey')
            disableIcon('Take_Coin', 'Coin')
            disableIcon('Take_Bag_Of_Gold', 'Bag')
        else:
            self.hide()

    def update_dungeon_chest(self, flag_found_dungeon_chest):
        self.flag_found_dungeon_chest = flag_found_dungeon_chest
