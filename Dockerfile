FROM centos:7

USER root

ENV PIP_DEFAULT_TIMEOUT 720
ENV VIRTUALENV_NO_DOWNLOAD=1
ENV VAULT_VERSION=1.7.2
ADD . /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT /usr/bin/entrypoint.sh
