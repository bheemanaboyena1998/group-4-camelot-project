from Item import *
from Place import *
from Character import *
from GameController import *

#Create the Dungeon with associated items and characters
Dungeon = Place('Dungeon', 'Dungeon')
Dungeon.enableIcon('Exit_Cell', 'Open', 'Dungeon.CellDoor', 'Leave your cell', 'true') 
Dungeon.enableIcon('Exit_Dungeon', 'Open', 'Dungeon.Door', 'Leave the dungeon', 'true') 
Dwight = Character('Dwight', 'B', 'Short_Full', 'LightArmour', 'Dungeon.Bed') 
Guard = Character('Guard', 'H', 'Long', 'HeavyArmour', 'Dungeon.Chair') 
Coin = Item('Coin', 'Coin', 'Dungeon.Chest')
ChestKey = Item('ChestKey', 'RedKey', 'Dungeon.DirtPile')
DungeonKey = Item('DungeonKey', 'BlueKey', 'Dungeon.Chest')
Dungeon_Potion = Item('Dungeon_Potion', 'RedPotion', 'Dungeon.Chest')
Bag_of_gold = Item('Bag_of_gold', 'Bag', 'Dungeon.Chest')

#Create the City with associated items and characters
City = Place('City', 'City')
Michael = Character('Michael', 'H', 'Short_Beard', 'Peasant', 'City.Bench')

#Create the Cottage with associated items and characters
Cottage = Place('Cottage', 'Cottage')
Bandit = Character('Bandit', 'F', 'Long', 'Bandit', 'Cottage')

#Create the Blacksmith with associated items and characters
Blacksmith = Place('Blacksmith', 'Blacksmith')
Creed = Character('Creed', 'D', 'Spiky', 'Merchant', 'Blacksmith')
Sword = Item('Sword', 'Sword', 'Creed') 
Spellbook = Item('Spellbook', 'Spellbook', 'Creed')
Blacksmith_Potion = Item('Blacksmith_Potion', 'PurplePotion', 'Creed') 

#Create the ForestPath with associated items and characters
ForestPath = Place('ForestPath', 'ForestPath')
Kevin = Character('Kevin', 'B', 'Short_Full', 'LightArmour', 'ForestPath') 
Kevin_sword = Item('Kevin_sword', 'Sword', 'Kevin')

#Create the Ruins with associated items and characters
Ruins = Place('Ruins', 'Ruins')
Jim = Character('Jim', 'F', 'Mage_Full', 'Warlock', 'Ruins.Throne')
Pam = Character('Pam', 'E', 'Long', 'Queen', 'Ruins.Chest') 
Jim_spellbook = Item('Jim_spellbook', 'Spellbook', 'Jim')

gc = GameController()
gc.setCameraFocus('Dwight') 
gc.showMenu()
