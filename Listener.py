from Action import *
from Leave import *
from GameController import *
import yaml
import logging

logger = logging.getLogger()
handler = logging.FileHandler('camelot.log')
logger.addHandler(handler)


def parse_initial_configuration():
    try:
        with open('/Users/rajeshbheemanaboyena/rajesh/camlot/camlot_test/test/rajesh-final-updates/Group-4-Camelot-Project/camlot_game_config.yaml') as file:
            inputConfig = yaml.safe_load(file)
            camlot_config = inputConfig.get("camlot_game").get("inputs")
    except Exception as exce:
        logger.error(exce)
    return camlot_config


def build_game_inputs(enable_params):
    posible_inputs = {}
    for val in enable_params.values():
        for object_list in val:
            camelot_input = "input " + object_list.get('params').get("action_name") + " " + object_list.get('name')
            posible_inputs[camelot_input] = object_list
    return posible_inputs


def wait_for_start_game(params):
    inputs = dict()
    game_inputs['input Close Narration'] = {'name': 'Inventory', 'params': {'action': 'hideNarration', 'action_name': 'Close Narration', 'message': 'Close Narration'}}
    game_inputs['input Key Interact'] = {'name': 'Interact', 'params': {'action': 'interact_lang', 'action_name': 'Close Narration', 'message': 'Close Narration'}}
    for input_option in params:
        camelot_input = 'input Selected ' + input_option
        inputs[camelot_input] = input_option
    inputs['input Close Credits'] = 'hideCredits'
    while True:
        camelot_input = input()
        if camelot_input in inputs.keys():
            eval('gc.' + inputs[camelot_input][0].lower() + inputs[camelot_input][1:] + '()')
        if inputs[camelot_input] == 'Start':
            break


camelot_config = parse_initial_configuration()
enable_icons = camelot_config.get("enable")
game_inputs = build_game_inputs(enable_icons)
print(len(game_inputs))
gc = GameController(camelot_config, game_inputs)
wait_for_start_game(camelot_config.get("show_options"))


while True:
    camelot_input = input()
    if camelot_input in gc.game_inputs.keys():
        fun_name = gc.game_inputs[camelot_input].get("params").get("action")
        eval('gc.' + fun_name + "()")

