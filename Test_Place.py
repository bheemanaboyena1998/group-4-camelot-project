from Place import *
import unittest
from io import StringIO
from unittest.mock import patch


class Test_Place(unittest.TestCase):

    def test_init(self):
        with patch('builtins.input', return_value='succeeded CreatePlace(Dungeon, Dungeon)'), patch('sys.stdout', new=StringIO()) as fake_out:
            place = Place('Dungeon', 'Dungeon')
            self.assertNotEqual(place, None)

    def test_enableIcon(self):
        with patch('builtins.input', return_value='succeeded CreatePlace(Dungeon, Dungeon)'), patch('sys.stdout', new=StringIO()) as fake_out:
            place = Place('Dungeon', 'Dungeon')
            self.assertNotEqual(place, None)

        with patch('builtins.input', return_value='succeeded EnableIcon("Take_Dungeon_Key", Take, Dungeon_Key, "Take Dungeon Key", True)'), patch('sys.stdout', new=StringIO()) as fake_out:
            enable = enableIcon('Take_Dungeon_Key', 'Take', 'Dungeon_Key', 'Take Dungeon Key', True)
            self.assertEqual(enable, True)

    def test_disableIcon(self):
        with patch('builtins.input', return_value='succeeded CreatePlace(Dungeon, Dungeon)'), patch('sys.stdout', new=StringIO()) as fake_out:
            place = Place('Dungeon', 'Dungeon')
            self.assertNotEqual(place, None)

        with patch('builtins.input', return_value='succeeded EnableIcon("Take_Dungeon_Key", Take, Dungeon_Key, "Take Dungeon Key", True)'), patch('sys.stdout', new=StringIO()) as fake_out:
            enable = enableIcon('Take_Dungeon_Key', 'Take', 'Dungeon_Key', 'Take Dungeon Key', True)
            self.assertEqual(enable, True)

        with patch('builtins.input', return_value='succeeded DisableIcon("Take_Dungeon_Key", Dungeon_Key)'), patch('sys.stdout', new=StringIO()) as fake_out:
            disable = disableIcon('Take_Dungeon_Key', 'Dungeon_Key')
            self.assertEqual(disable, True)


if __name__ == "__main__":
    unittest.main()
