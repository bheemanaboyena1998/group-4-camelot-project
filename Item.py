from Action import *
    
class Item:
    def __init__(self, item_name, item):
        self.item_name = item_name
        self.item = item
        action('CreateItem(' + self.item_name + ',' + self.item+')')

    def set_position(self, position):
        action('SetPosition('+self.item_name+','+position+')')
        
