from unittest import TestCase
from unittest.mock import patch, Mock

class TestActions(TestCase):

    @patch('Action.Take', return_value='succeeded Take(Dwight, CellKey, Dungeon.RoundTable)')
    def test_take(self, Take):
        self.assertEqual(Take('Dwight', 'CellKey', 'Dungeon.RoundTable'), 'succeeded Take(Dwight, CellKey, Dungeon.RoundTable)')

    @patch('Action.Give', return_value='succeeded Give(Dwight, dungeon_potion, Guard)')
    def test_give(self, Give):
        self.assertEqual(Give('Dwight', 'dungeon_potion', 'Guard'), 'succeeded Give(Dwight, dungeon_potion, Guard)')

    @patch('Action.Leave', return_value='succeeded Exit(Dwight, Dungeon.Door, true)')
    def test_leave(self, Leave):
        self.assertEqual(Leave('Dwight', 'Dungeon.Door'), 'succeeded Exit(Dwight, Dungeon.Door, true)')

    @patch('Action.Open', return_value='succeeded OpenFurniture(Dwight, Dungeon.CellDoor)')
    def test_open(self, Open):
        self.assertEqual(Open('Dwight', 'Dungeon.CellDoor'), 'succeeded OpenFurniture(Dwight, Dungeon.CellDoor)')

    @patch('Action.Close', return_value='succeeded CloseFurniture(Guard, Dungeon.CellDoor)')
    def test_close(self, Close):
        self.assertEqual(Close('Guard', 'Dungeon.CellDoor'), 'succeeded CloseFurniture(Guard, Dungeon.CellDoor)')

    @patch('Action.Fight', return_value='succeeded Attack(Bandit, Dwight, false)')
    def test_fight(self, Fight):
        self.assertEqual(Fight('Bandit', 'Dwight'), 'succeeded Attack(Bandit, Dwight, false)')

    @patch('Action.Put', return_value=dict())
    def test_put(self, Put):
        self.assertEqual(Put(), dict())

    @patch('Action.Buy', return_value=dict())
    def test_buy(self, Buy):
        self.assertEqual(Buy(), dict())

    @patch('Action.Die', return_value='succeeded Die(Guard)')
    def test_die(self, Die):
        self.assertEqual(Die('Guard'), 'succeeded Die(Guard)')

