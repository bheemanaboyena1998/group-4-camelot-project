from Action import *

class TeluguDialog:

        def __init__(self):
                pass

        def setLeft(self, character):
                action(f'SetLeft({character.name})')

        def setRight(self, character):
                action(f'SetRight({character.name})')

        def showDialog(self):
                action('ShowDialog()')

        def setDialog(self, message):
                action(f'SetDialog("{message}")')
        
        def clearDialog(self):
                action('ClearDialog()')
    
        def hideDialog(self):
                action('HideDialog()')

        def showNarration(self):
                action('ShowNarration()')

        def setNarration(self, message):
                action(f'SetNarration("{message}")')

        def guardBeforePotion(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Guard: vesikchaku vellipoo \\n[Leave | vellipo]')
                self.showDialog()
                choice = waitForChoice(['Leave'])
                if choice == 'Leave':
                        self.hideDialog()
                        return False

        def guardAfterPotion(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Guard: What do you have there\\n[Nothing | Nothing I want to show you]\\n[Offer | I found a strength potion. Do you want it?]')
                self.showDialog()
                choice = waitForChoice(['Nothing', 'Offer'])
                self.clearDialog()
                if choice == 'Nothing':
                        self.setDialog('Guard: You bore me\\n[Leave | Leave]')
                        choice2 = waitForChoice(['Leave'])
                        if choice2 == 'Leave':
                                self.hideDialog()
                                return False
                elif choice == 'Offer':
                        self.setDialog('Guard: Sure, Ill try it\\n[Give | Give potion to guard]')
                        choice2 = waitForChoice(['Give'])
                        if choice2 == 'Give':
                                self.hideDialog()
                                return True

        def jimBeforeWeapon(self, character_left, character_right): 
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Jim: Thank you, Dwight, for returning the one coin to me. I shall use its power to make the whole of Scranton mine, once and for all! Die now...\\n[Begin | Begin Fight!]')
                self.showDialog()
                choice = waitForChoice(['Begin'])
                if choice == 'Begin':
                        self.hideDialog()
        def jimAfterWeapon(self, character_left, character_right): 
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Jim: At long last, Dwight, you have finally returned. Let us now decide the fate of our world, Scranton, and the fate of Pam. Prepare yourself!\\n[Prepare | Prepare for Battle]')
                self.showDialog()
                choice = waitForChoice(['Prepare'])
                self.clearDialog()
                if choice == 'Prepare':
                        self.setDialog('Dwight: I will destroy your coin, Jim, save Scranton, and free Pam from your evil grasp!\\n[Begin | Begin Fight!]')
                        self.showDialog()
                        choice2 = waitForChoice(['Begin'])
                        if choice2 == 'Begin':
                                self.hideDialog()

        def jimDefeated(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Jim: I never thought I would see the day... You have bested me, Dwight. Place the coin upon the altar, where I forged the most powerful object in all of Scranton, and this shall all be over...\\n[Destroy | Destroy the one coin]')
                self.showDialog()
                choice = waitForChoice(['Destroy'])
                if choice == 'Destroy':
                        self.hideDialog()
                        self.setNarration('You have weakened the dark lord, Jim! Walk over to the altar, drop the ring, and Jim will be destroyed!')
                        self.showNarration()


        def talkPam(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Pam: Dwight! You are here! Jim will not allow me to leave... You must defeat him and destroy the coin to save me and Scranton!\\n[Fight | Go Fight Jim]')
                self.showDialog()
                choice = waitForChoice(['Fight'])
                if choice == 'Fight':
                        self.hideDialog()

        def talkBandit(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Bandit: Give me coin... I will kill you \\n[Fight | Fight Bandit]')
                self.showDialog()
                choice = waitForChoice(['Fight'])
                if choice == 'Fight':
                        self.hideDialog()
                        return choice

        def loseMessage(self):
                self.setNarration('You have been defeated by the dark lord, Jim. He has regained his coin, and now has the power to enslave all of Scranton...')
                self.showNarration()

        def talkKevinWeapon(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Kevin: Give me the coin, or I will take it! Jim will have his coin... \\n[Fight | Fight Kevin]')
                self.showDialog()
                choice = waitForChoice(['Fight'])
                self.clearDialog()
                if choice == 'Fight':
                    self.hideDialog()

        def talkKevinNoWeapon(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Kevin: Give me the coin, or I will take it! \\n[Bribe | Bribe Kevin]')
                self.showDialog()
                choice = waitForChoice(['Bribe'])
                self.clearDialog()
                if choice == 'Bribe':
                        self.setDialog('Kevin: Thank you for the gold. I cannot wait to buy the ingredients for my famous chili! What were we talking about again?...\\n[Ok | Ok...]')
                        self.showDialog()
                        choice2 = waitForChoice(['Ok'])
                        if choice2 == 'Ok':
                                self.hideDialog()

        def talkMichael(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Michael: Have you seen the coin? It is mine. Give me the coin. \\n[One | Here is the coin] \\n[Two | I have not seen the coin]')
                self.showDialog()
                choice = waitForChoice(['One', 'Two'])
                self.clearDialog()
                if choice == 'One':
                    self.hideDialog()
                    return True
                elif choice == 'Two':
                    self.clearDialog()
                    self.setDialog("Michael: Then find it! Check with the Blacksmith. \\n[Okay|Okay]")
                    choice2 = waitForChoice(['Okay'])
                    if choice2 == 'Okay':
                        self.hideDialog()
                        return False

        def talkToMichael_after_weapon(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Michael: Ohh.. you brought weapon. \\n Go and fight with Bandit \\n[Okay|Okay]')
                self.showDialog()
                choice = waitForChoice(['Okay'])
                if choice == 'Okay':
                    self.hideDialog()

        def talkCreedGold(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Creed: Welcome to the city\'s Blacksmith! My name is Creed, local wizard and connisseur of spells. I can help with any type of soldier in terms of weapons. Do you want steel, spellwork, or poison to defeat your enemies?\\n[Sword | Test your might with steel]\\n[PurplePotion | Posion your enemies before they can attack]\\n[Spellbook | Cast powerful spells with ancient knowledge.]')
                self.showDialog()
                choice = waitForChoice(['Sword', 'PurplePotion', 'Spellbook' ])
                self.clearDialog()
                if choice == 'Sword' or choice == 'PurplePotion' or choice == 'Spellbook':
                    self.setDialog('Creed: An excellent choice! Please pay 1 bag of gold for your purchase, and you may be on your way\\n[Give | Give bag of gold] \\n[Refuse | Do not give gold]')
                    self.showDialog()
                    choice2 = waitForChoice(['Give', 'Refuse'])
                    self.hideDialog()
                    if choice2 == 'Give':
                        return True, choice

        def talkCreedNoGold(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Creed: Welcome to the city\'s Blacksmith! My name is Creed, local wizard and connisseur of spells. I wish I could help you, but I see you have no gold. Please come back with money to obtain a weapon for your travels\\n[Leave | Leave]')
                self.showDialog()
                choice = waitForChoice(['Leave'])
                if choice == 'Leave':
                    self.hideDialog()

        def pamWinGame(self, character_left, character_right):
                self.setLeft(character_left)
                self.setRight(character_right)
                self.setDialog('Pam: Dwight, you did it! The coin is destroyed, Jim is vanquished, and I am freed from that evil man\'s grasp!\\n[Continue | Continue]')
                self.showDialog()
                choice = waitForChoice(['Continue'])
                if choice == 'Continue':
                        self.hideDialog()
