from Action import *
class Character:
    def __init__(self, name, body_type):
        self.name = name
        action(f'CreateCharacter({self.name}, {body_type})')

    def set_outfit(self, hair_style, outfit):
        action(f'SetClothing({self.name}, {outfit})')
        action(f'SetHairStyle({self.name}, {hair_style})')

    def set_position(self, position):
        action(f'SetPosition({self.name}, {position})')


def setCameraFocus(entity_name):
    action(f'SetCameraFocus({entity_name})')